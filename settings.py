
"""
Módulo para configurações do R2
"""
import os

TOKEN = os.environ.get('TOKEN', '')
DESTROYER_URL = os.environ.get('STAR_DESTROYER', '')
GENERAL_CHANNEL = os.environ.get('GENERAL_CHANNEL', '')
R2ID = os.environ.get('R2_ID', '')
GITLAB_REPO = os.environ.get('GITLAB_REPO', '')
